/* TinyWM is written by Nick Welch <nick@incise.org> in 2005 & 2011.
 * Turkwm is based on TinyWM developed by Ali Rıza KESKİN in 2019-?
 *
 * This software is in the public domain
 * and is provided AS IS, with NO WARRANTY. */

#include <X11/Xlib.h>
#include <stdlib.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))

int main(void)
{
	system("dbus-update-activation-environment --all > /dev/null 2>&1 ||:");
	system("exec $STARTUP &");
	Display * dpy;
	XWindowAttributes attr;
	XButtonEvent start;
	XEvent ev;
	int xdiff=0;
	int ydiff=0;
	while (1==1){
		if(!(dpy = XOpenDisplay(0x0))) return 1;
			start.subwindow=None;
			XWindowAttributes ra;
			XGetWindowAttributes(dpy, DefaultRootWindow(dpy), &ra);
			XGrabKey(dpy, XKeysymToKeycode(dpy, XStringToKeysym("F1")), Mod4Mask,
				XDefaultRootWindow(dpy), True, GrabModeAsync, GrabModeAsync);
			XGrabKey(dpy, XKeysymToKeycode(dpy, XStringToKeysym("F4")), Mod4Mask,
				XDefaultRootWindow(dpy), True, GrabModeAsync, GrabModeAsync);
			XGrabKey(dpy, XKeysymToKeycode(dpy, XStringToKeysym("F6")), Mod4Mask,
				XDefaultRootWindow(dpy), True, GrabModeAsync, GrabModeAsync);
			XGrabKey(dpy, XKeysymToKeycode(dpy, XStringToKeysym("F5")), Mod4Mask,
				XDefaultRootWindow(dpy), True, GrabModeAsync, GrabModeAsync);
			XGrabKey(dpy, XKeysymToKeycode(dpy, XStringToKeysym("F12")), Mod4Mask,
				XDefaultRootWindow(dpy), True, GrabModeAsync, GrabModeAsync);
			XGrabKey(dpy, XKeysymToKeycode(dpy, XStringToKeysym("F3")), Mod4Mask,
				XDefaultRootWindow(dpy), True, GrabModeAsync, GrabModeAsync);
			XGrabKey(dpy, XKeysymToKeycode(dpy, XStringToKeysym("F2")), Mod4Mask,
				XDefaultRootWindow(dpy), True, GrabModeAsync, GrabModeAsync);
			XGrabButton(dpy, 1, Mod4Mask, XDefaultRootWindow(dpy), True,
				ButtonPressMask|ButtonReleaseMask|PointerMotionMask, GrabModeAsync, GrabModeAsync, None, None);
			XGrabButton(dpy, 3, Mod4Mask, XDefaultRootWindow(dpy), True,
				ButtonPressMask|ButtonReleaseMask|PointerMotionMask, GrabModeAsync, GrabModeAsync, None, None);
			for(;;){
				XSync(dpy,False);
				XFlush(dpy);
				XNextEvent(dpy, &ev);
				if(ev.type == KeyPress){
					XSetInputFocus(dpy,XDefaultRootWindow(dpy),RevertToParent,CurrentTime);
					if(ev.xbutton.button == XKeysymToKeycode(dpy,XStringToKeysym("F4")) && ev.xkey.subwindow != None){
						XEvent event;
						event.xclient.type = ClientMessage;
						event.xclient.window = ev.xkey.subwindow;
						event.xclient.message_type = XInternAtom(dpy, "WM_PROTOCOLS", True);
						event.xclient.format = 32;
						event.xclient.data.l[0] = XInternAtom(dpy, "WM_DELETE_WINDOW", False);
						event.xclient.data.l[1] = CurrentTime;
						XSendEvent(dpy, ev.xkey.subwindow, False, NoEventMask, &event);
					}else if(ev.xbutton.button == XKeysymToKeycode(dpy,XStringToKeysym("F5")) && ev.xkey.subwindow != None){
						XMoveResizeWindow(dpy, ev.xkey.subwindow,0,0,ra.width,ra.height);
					}else if(ev.xbutton.button == XKeysymToKeycode(dpy,XStringToKeysym("F6")) && ev.xkey.subwindow != None){
						XDestroyWindow(dpy,ev.xkey.subwindow);
					}else if(ev.xbutton.button == XKeysymToKeycode(dpy,XStringToKeysym("F3"))){
						 system("exec $FILEMANAGER &");
					}else if(ev.xbutton.button == XKeysymToKeycode(dpy,XStringToKeysym("F12"))){
						 XCloseDisplay(dpy);
					}else if(ev.xbutton.button == XKeysymToKeycode(dpy,XStringToKeysym("F2"))){
						 system("exec $APPFINDER &");
					}else if(ev.xbutton.button == XKeysymToKeycode(dpy,XStringToKeysym("F1"))){
						 system("exec $TERMINAL &");
					}else if (ev.xbutton.subwindow != None){
						XRaiseWindow(dpy, ev.xbutton.subwindow);
						XSetInputFocus(dpy,ev.xbutton.subwindow,RevertToParent,CurrentTime);
					}
					start.subwindow=None;
					start = ev.xbutton;
				}else if(ev.type == ButtonPress && ev.xbutton.subwindow != None){
					XSetInputFocus(dpy,XDefaultRootWindow(dpy),RevertToParent,CurrentTime);
					XGetWindowAttributes(dpy, ev.xbutton.subwindow, &attr);
					XRaiseWindow(dpy, ev.xbutton.subwindow);
					XSetInputFocus(dpy,ev.xbutton.subwindow,RevertToParent,CurrentTime);
					start.subwindow=None;
					start = ev.xbutton;
				}else if(ev.type == MotionNotify && start.subwindow != None){
					xdiff = ev.xbutton.x_root - start.x_root;
					ydiff = ev.xbutton.y_root - start.y_root;
					XRaiseWindow(dpy, start.subwindow);
					XSetInputFocus(dpy,start.subwindow,RevertToParent,CurrentTime);
					XMoveResizeWindow(dpy, start.subwindow,
						attr.x + (start.button==1 ? xdiff : 0),
						attr.y + (start.button==1 ? ydiff : 0),
						MAX(1, attr.width + (start.button==3 ? xdiff : 0)),
						MAX(1, attr.height + (start.button==3 ? ydiff : 0)));
				}
			}
		}
		return 0;
}
