PREFIX?=/usr/X11R6
CFLAGS?=-Os -pedantic -Wall

all:
	$(CC) $(CFLAGS) -I$(PREFIX)/include turkwm.c -L$(PREFIX)/lib -lX11 -o turkwm

clean:
	rm -f miniwm
install:
	cp -prf turk-oturum /usr/bin/turk-oturum
	mv turkwm /usr/bin/turkwm
	cp -prf turkwm.desktop /usr/share/xsessions/
